


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body style="margin-left:20px; margin-right:20px">
<h1>Numbers to Words</h1>
    <form method="POST" action="/store" >
   {{ csrf_field() }}
      <div  class="mb-3">
        <label class="form-label">Number</label>
        <input  type="number" name="number" class="form-control"  >

<input id="rateText" name="rateText" value="PHP" type ="hidden"/>
<p>BASE CURRENCY: EURO</p>


<label>Convert To</label>
<select id = "rateSelection"  name="rates">
@foreach($rates as $key => $r)
<option name="{{$key}}"  value={{$r}}>{{$key}}</option>
@endforeach
</select>

<br> <button type="submit"  class="btn btn-primary">Submit</button>
              </div>
<hr>
<label><strong>Default Currency</strong></label>
<br>
<span id="d1">{{$defaultNumber ?? ''}}</span> <span>EUR</span>
<br>
<label><strong>Default Currency in Words</strong></label>
<br>
{{ $defaultCurrencyToWord ?? '' }} <span>EUR</span>

<hr>


<label><strong>Converted Currency</strong></label>
<br>

<span id="d2">{{$converted ?? ''}} </span> <span>{{$convertedRate ?? ''}}</span>
<br>
<label><strong>Converted Currency in Words</strong></label>
<br>
{{$convertedCurrencyToWords ?? ''}} 


    </form>


        <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
<script>

toNumber()
document.getElementById("rateSelection").value = "PHP";
var rs = document.getElementById("rateSelection");
rs.options["PHP"].defaultSelected = true;

function updateRates(){
   var sel = document.getElementById("rateSelection");
var text = sel.options[sel.selectedIndex].text;
document.getElementById("rateText").value = text;

}

document.querySelector("#rateSelection").addEventListener("change",
  () => {
  updateRates();
} 

)

  
function toNumber(){
  document.getElementById("d1").innerHTML = parseInt(document.getElementById("d1").innerHTML).toLocaleString();
document.getElementById("d2").innerHTML = parseInt(document.getElementById("d2").innerHTML).toLocaleString();



}
</script>


  </body>
</html>

