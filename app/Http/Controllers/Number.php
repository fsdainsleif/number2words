<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Number extends Controller
{
    //
    public function index()
    {
        //generic
        //

        return view("number", ["rates" => $this->getRates()]);
    }

    public function getRates()
    {
        $url =
            "http://api.exchangeratesapi.io/v1/latest?access_key=" .
            env("MONEY") .
            "";
        $response = Http::get($url);
        $response1 = Http::accept("application/json")->get($url);
        $rates = $response1->object()->rates;
        return $rates;
    }

    public function convert()
    {
        return view("number");
    }

    public function tens($val)
    {
        $newStr = str_split($val);
        $secondValue = $this->ones($newStr[1]);
        if ($newStr[0] == 1) {
            switch ($val) {
                case 10:
                    return "Ten";
                    break;
                case 11:
                    return "Eleven";
                    break;
                case 12:
                    return "Twelve";
                    break;
                case 13:
                    return "Thirteen";
                    break;
                case 14:
                    return "Fourteen";
                    break;
                case 15:
                    return "Fifteen";
                    break;
                case 16:
                    return "Sixteen";
                    break;
                case 17:
                    return "Seventeen";
                    break;

                case 18:
                    return "Eighteen";
                    break;
                case 19:
                    return "Ninteen";
                    break;
            }
        }
        $full = "";
        switch ($newStr[0]) {
            case 2:
                $full = "Twenty ";
                break;
            case 3:
                $full = "Thirty ";
                break;
            case 4:
                $full = "Fourty ";
                break;
            case 5:
                $full = "Fifty ";
                break;
            case 6:
                $full = "Sixty ";
                break;
            case 7:
                $full = "Seventy ";
                break;
            case 8:
                $full = "Eighty ";
                break;
            case 9:
                $full = "Ninety ";
                break;
            default:
                $full = "";
                break;
        }

        return $full . $secondValue;
    }

    public function ones($val)
    {
        switch ($val) {
            case 1:
                return "One";
                break;
            case 2:
                return "Two";
                break;
            case 3:
                return "Three";
                break;
            case 4:
                return "Four";
                break;
            case 5:
                return "Five";
                break;
            case 6:
                return "Six";
                break;
            case 7:
                return "Seven";
                break;
            case 8:
                return "Eight";
                break;
            case 9:
                return "Nine";
                break;
        }
    }
    public function store(Request $request)
    {
        $convertedCurrency = (int) ($request->number * $request->rates);
        return view("number", [
            "defaultCurrencyToWord" => $this->NumberToWords($request->number),
            "defaultNumber" => $request->number,
            "rates" => $this->getRates(),
            "converted" => $convertedCurrency,
            "convertedRate" => $request->rateText,
            "convertedCurrencyToWords" =>
                $this->NumberToWords($convertedCurrency) .
                " " .
                $request->rateText,
        ]);
    }
    public function NumberToWords($value)
    {
        //string
        //
        //
        $str = strlen($value);
        $numArray = str_split($value);

        $finalArray = [];
        $tryCount = 0;
        $i = strlen($value);
        while ($i >= 1) {
            $tryCount++;
            $checkIfAllZero = false;
            if ($i >= 3) {
                $newStr = "";
                $newStr .= $this->ones($numArray[$i - 3]);
                if ($newStr != "") {
                    $newStr .= " hundred ";
                }

                if (
                    $numArray[$i - 3] != 0 ||
                    $numArray[$i - 2] != 0 ||
                    $numArray[$i - 1] != 0
                ) {
                    $checkIfAllZero = true;
                }

                $newStr .= $this->tens($numArray[$i - 2] . $numArray[$i - 1]);

                array_unshift($finalArray, $newStr);

                $i -= 3;
            } elseif ($i == 2) {
                $newStr = "";
                $newStr .= $this->tens($numArray[$i - 2] . $numArray[$i - 1]);
                if ($numArray[$i - 2] != 0 || $numArray[$i - 1] != 0) {
                    $checkIfAllZero = true;
                }

                array_unshift($finalArray, $newStr);
                $i -= 2;
            } else {
                $newStr = "";
                $newStr .= $this->ones($numArray[0]);
                if ($numArray[0] != 0) {
                    $checkIfAllZero = true;
                }

                array_unshift($finalArray, $newStr);
                $i -= 1;
            }

            if ($tryCount == 2 && $checkIfAllZero) {
                array_unshift($finalArray, "Thousand");
                $finalArray = $this->swap($finalArray);
            } elseif ($tryCount == 3 && $checkIfAllZero) {
                array_unshift($finalArray, "Million");
                $finalArray = $this->swap($finalArray);
            } elseif ($tryCount == 4 && $checkIfAllZero) {
                array_unshift($finalArray, "Billion");

                $finalArray = $this->swap($finalArray);
            } elseif ($tryCount == 5 && $checkIfAllZero) {
                array_unshift($finalArray, "Trillion");

                $finalArray = $this->swap($finalArray);
            }
            $checkIfAllZero = false;
            //THOUSANDS OR MILLIONS
        }
        return join(" ", $finalArray);
    }
    public function swap($arr)
    {
        $temp = $arr[1];
        $arr[1] = $arr[0];
        $arr[0] = $temp;
        return $arr;
    }
}
